#
# This file is part of ucs-migration-toolkit
#
# ucs-migration-toolkit is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ucs-migration-toolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ucs-migration-toolkit.  
# 
# If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2016-2017 SURFsara

# Python imports
from __future__ import print_function

# Python imports
import base64
import string
import random

def _print(*args, **kwargs):
    '''Just a simple wrapper function for print, so I only need to 
    import future stuff at one place'''
    print(*args, **kwargs)

def to_base64(string, args):
    return base64.b64encode(string)

def split(string, args):
    parts = args.split(';')
    return string.split(parts[0])[int(parts[1])]

def append(string, args):
    pass

def replace(string, args):
    parts = args.split(';')
    return string.replace(parts[0], parts[1])

def gen_password():
    chars = chars = string.ascii_letters + string.digits
    return ''.join(random.choice(chars) for _ in range(32))
